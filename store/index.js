// vuex 状态管理
import Vue from 'vue'
import Vuex from 'vuex'

// 挂载Vuex
Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		hasLogin: false,
		access_token: 'eyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOjMxMDM2NzM4NTIwMDk2MCwiYWNjb3VudCI6IjIwMjAwMDEiLCJ1c2VyS2V5IjoiMSIsInN1YiI6IjMxMDM2NzM4NTIwMDk2MCIsImlhdCI6MTYyMjI5MDM5MSwiZXhwIjoxNjIyMzc2NzkxfQ.R_hKSuKkdP6VQdsnC4iZaEkoBxXC5LE4xZhiWYv6RJ7rLHb_cPO5J7GkCAIvaTq3yD6yFgZG6cuz_yJ9oeCRhg',
		userInfo: {},
		quesArr: [],
		base_url: "http://61.132.233.226:9001/escloud/ws2",
		examDetail: {}, // 考试详情
		storeInfo: {
			storeName: '',
			storeId: '',
		},
		queTypeArr:[0,0,0,0,0]
	},
	mutations: {
		setAccessToken(state, data) {
			state.access_token = data
		},
		setUserInfo(state, data) {
			state.userInfo = data
		},
		setExamDetail(state, data) {
			state.examDetail = data
		},
		// 重置所有state
		resetAll(state) {
			Object.assign(state, initState); //用原数据进行还原
			// localStorage.removeItem('token') 
		},
	},
	actions: {}
})

// 数据源，请讲state写在此处，为的是退出登录功能重置所有state
// 调用store数据时按原语法正常调用
// let that = this
// let initState
// uni.getStorage({
// 	key: 'state',
// 	success: function(res) {
// 		initState = res
// 	},
// 	fail: function(res) {
// 		initState = {
// 			// 存放的键值对就是所要管理的状态
// 			hasLogin: false,
// 			access_token: 'eyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOjMxMDM2NzM4NTIwMDk2MCwiYWNjb3VudCI6IjIwMjAwMDEiLCJ1c2VyS2V5IjoiMSIsInN1YiI6IjMxMDM2NzM4NTIwMDk2MCIsImlhdCI6MTYxODgyNTU2MSwiZXhwIjoxNjE4OTExOTYxfQ.fzaXtIrK4DiN0xVtYMbbFkd4DTQlmvTROZu0mD9mNO5xrJL9as16UKm2CFV_xquQRF84KuQaYfCSE6Vl-hyF_A',
// 			userInfo: {},
// 			quesArr: [],
// 			base_url: "http://61.132.233.226:9001/escloud/ws2",
// 			examDetail: {}, // 考试详情
// 			questionList: [], // 考试考题列表
// 			storeInfo: {
// 				storeName: '',
// 				storeId: '',
// 			},
// 		}
// 	}
// });


// let initState = sessionStorage.getItem('state') ? JSON.parse(sessionStorage.getItem('state')): {
// 	// 存放的键值对就是所要管理的状态
// 	hasLogin:false ,
// 	access_token: 'eyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOjMxMDM2NzM4NTIwMDk2MCwiYWNjb3VudCI6IjIwMjAwMDEiLCJ1c2VyS2V5IjoiMSIsInN1YiI6IjMxMDM2NzM4NTIwMDk2MCIsImlhdCI6MTYxODgyNTU2MSwiZXhwIjoxNjE4OTExOTYxfQ.fzaXtIrK4DiN0xVtYMbbFkd4DTQlmvTROZu0mD9mNO5xrJL9as16UKm2CFV_xquQRF84KuQaYfCSE6Vl-hyF_A',
// 	userInfo: {},
// 	// bankArr: [],
// 	quesArr: [],
// 	base_url:"http://61.132.233.226:9001/escloud/ws2",
// 	examDetail:{},   // 考试详情
// 	questionList:[], // 考试考题列表
// 	storeInfo:{
// 		storeName:'',
// 		storeId:'',
// 	},
// }
// let cloneDeep = (data) => { //深度copy一份数据
// 	return JSON.parse(JSON.stringify(data))
// }
// // 创建VueX对象
// // state:sessionStorage.getItem('state') ? JSON.parse(sessionStorage.getItem('state')): 
// const store = new Vuex.Store({
// 	//这里拿到的是copy后的数据
// 	state: cloneDeep(initState),
// 	mutations: {
// 		setAccessToken(state, data) {
// 			state.access_token = data.values
// 		},
// 		setUserInfo(state, data) {
// 			state.userInfo = data.values
// 		},
// 		// 重置所有state
// 		resetAll(state) {
// 			Object.assign(state, initState); //用原数据进行还原
// 			// localStorage.removeItem('token') 
// 		},
// 	},
// 	actions: {}
// })
export default store
