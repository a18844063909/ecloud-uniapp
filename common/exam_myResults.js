const myResultList = {
	"total": 2,
	"size": 10,
	"pages": 1,
	"current": 1,
	"records": [{
		"id": "3302",
		"examId": "33021",
		"examName": "宏观经济学", //…………………………考试名称 
		"subjectName": "演示学科", //…………………………学科名称 
		"enterTime": "2023-01-05 16:18:53",  //…………………………进入时间 
		"handPaperTime": "2023-01-05 17:19:00",  //…………………………交卷时间 
		"examIp": "127.0.0.1",  //…………………………考试IP 
		"examScore": 0.0,  //…………………………得分 
		"totalQueNum": 10, //…………………………总题数 
		"answerQueNum": 2, //…………………………答题数 
		"rightQueNum": 0, //…………………………正确数 
		"errorQueNum": 2, //…………………………错误数 
		"examTime": -1, //…………………………考试时长(min， - 1 代表不限时长)
		"totalScore": 100.0, //…………………………总分 
		"passScore": 60.0, //…………………………及格分 
		"answerTime": "61分钟", //…………………………考试用时
		"scoreState":1 // 1未发布 2已发布
	}]
}
module.exports = {
	myResultList: myResultList //前一个userInfo是将后一个userInfo对象数组暴露出去的命名的名字，自己定义的
}
