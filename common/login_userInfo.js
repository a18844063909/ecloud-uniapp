const userInfo = {
	"id": "310367385200960",
	"studentNo": "2020001",
	"clazzName": "财务1班",
	"email": "",
	"phone": "",
	"nickName": "爱学习的峰峰",
	"realName": "李易峰",
	"photo": null,
	"sex": 1, // ： 1:男, 2:女 
	"address": "",
	"bir": "", // 生日(yyyy - MM - dd)
	"teacherName": "",
	"roleName": "学生",
	"token":"sfhajfhqi123",
	// "avatar": "@/static/portrait2.jpeg"
	"avatar": "https://img0.baidu.com/it/u=3036337598,3210476231&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=784"
}
// [{
// 		id: "1",
// 		name: "张三",
// 		position: "../../static/ren/tx1.jpg",
// 		identity: "管理员",
// 	},{
// 		id: "2",
// 		name: "李四",
// 		position: "../../static/ren/tx2.jpg",
// 		identity: "游客"
// 	},
// 	{
// 		id: "3",
// 		name: "陈五",
// 		position: "../../static/ren/tx3.jpg",
// 		identity: "游客"
// 	}
// ]
module.exports = {
	userInfo: userInfo //前一个userInfo是将后一个userInfo对象数组暴露出去的命名的名字，自己定义的
}
