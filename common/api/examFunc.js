import store from '@/store/index.js'; //需要引入store
// Text页面对考题列表questionList数据预处理
export const ModifyQuesList = function(quesModules) {
	// store.state.questionList = []
	var questionList = []
	for (let i = 0; i < quesModules.length; i++) {
		var quesType = quesModules[i].template
		store.state.queTypeArr[quesType-1] = 1
		for (let j = 0; j < quesModules[i].questions.length; j++) {
			// 解决theoryExamAnswer可能为null的问题，把它赋值为字符串
			quesModules[i].questions[j].tempAnswer = "" //暂存每题的答案
			quesModules[i].questions[j].template = quesModules[i].template //防止template初始值是null
			quesModules[i].questions[j].recordId = store.state.examDetail
				.recordId //搬移recordId的位置
			quesModules[i].questions[j].blankArr = [] //新建blankArr属性，临时保存填空题答案
			quesModules[i].questions[j].dualChooseArr = [] //新建blankArr属性，临时保存填空题答案
			questionList.push(quesModules[i].questions[j])
		}
	}
	store.state.questionList = questionList
}

// Text页面对考题列表题库数据预处理
export const ModifyStoreQuesList = function(quesList) {
	var questionList = []
	for (let j = 0; j < quesList.length; j++) {
		// 解决theoryExamAnswer可能为null的问题，把它赋值为字符串
		quesList[j].tempAnswer = "" //暂存每题的答案
		// quesList[j].template = quesModules[i].template //防止template初始值是null
		// quesList[j].recordId = store.state.examDetail
		// 	.recordId //搬移recordId的位置
		quesList[j].blankArr = [] //新建blankArr属性，临时保存填空题答案
		quesList[j].dualChooseArr = [] //新建blankArr属性，临时保存填空题答案
		questionList.push(quesList[j])
	}
	store.state.questionList = questionList
}

// pracBankPage页面对数据进行预处理 包括题库练习和错题练习
export const ModifyErrorQuesList = function(APIquesList){
	var questionList = []
	for (let j = 0; j < APIquesList.length; j++) {
		// 解决theoryExamAnswer可能为null的问题，把它赋值为字符串
		APIquesList[j].tempAnswer = "" //暂存每题的答案
		APIquesList[j].isReviewed = false //暂存每题的答案
		// quesList[j].recordId = store.state.examDetail
		// 	.recordId //搬移recordId的位置
		APIquesList[j].blankArr = [] //新建blankArr属性，临时保存填空题答案
		APIquesList[j].dualChooseArr = [] //新建blankArr属性，临时保存填空题答案
	
		//------------------- -----
		// 解决theoryExamAnswer可能为null的问题，把它赋值为字符串
		// if (APIquesList[j].theoryExamAnswer == null) {
		// 	APIquesList[j].unAnswered = true //标记此题未答
		// }
		// APIquesList[j].template = quesModules[i].template //防止template初始值是null
		// APIquesList[j].tempAnswer = ''
		APIquesList[j].rightAnswer = ''
		APIquesList[j].wrongArr = [] // 保存选中的错误的选项
		APIquesList[j].isRight = '' // 保存选中的错误的选项
	
		// 遍历选项得出正确答案和用户答案
		if (APIquesList[j].template == 1 || APIquesList[j].template ==
			2) {
			// 单选题与多选题
			// if (APIquesList[j].unAnswered) {
			// 	APIquesList[j].theoryExamAnswer = {
			// 		optAnswer: "未作答"
			// 	}
			// }
	
			// 用户答案
			// APIquesList[j].tempAnswer = APIquesList[j]
			// 	.theoryExamAnswer.optAnswer
	
			// 赋值选择题正确答案
			for (let k = 0; k < APIquesList[j].toptions.length; k++) {
				// 获取选择题的正确答案
				if (APIquesList[j].toptions[k].optAnswer == 1) {
					if (APIquesList[j].rightAnswer == '') {
						APIquesList[j].rightAnswer = APIquesList[j]
							.toptions[k].optLetter
					} else {
						APIquesList[j].rightAnswer += ',' + APIquesList[j].toptions[k].optLetter
					}
				}
			}
	
			// 判断题目错选了哪一项
			// 		var rightArr = APIquesList[j].rightAnswer.split(',')
			// 		var userArr = APIquesList[j].tempAnswer.split(',')
			// 		APIquesList[j].wrongArr = [] // 保存选中的错误的选项
	
			// 		for (var item of userArr) {
			// 			// 用户的该答案不在正确答案数组在
			// 			if (rightArr.indexOf(item) < 0) {
			// 				APIquesList[j].wrongArr.push(item)
			// 			}
			// 		}
	
		} else if (APIquesList[j].template == 3) {
			// 3.判断题
			APIquesList[j].wrongArr = ''
			// if (APIquesList[j].unAnswered) {
			// 	APIquesList[j].theoryExamAnswer = {
			// 		optAnswer: "未作答"
			// 	}
			// }
			// 题目已经作答 获取用户判断题答案 0错误 1正确
			// APIquesList[j].tempAnswer = APIquesList[j]
			// 	.theoryExamAnswer.optAnswer
	
			// 为判断题的答案赋值
			if (APIquesList[j].toptions[0].optAnswer == 0) {
				APIquesList[j].rightAnswer = "错误"
			} else {
				APIquesList[j].rightAnswer = "正确"
			}
	
			// 判断题目错选了哪一项
			// if (APIquesList[j].tempAnswer != APIquesList[j].options[0]
			// 	.optAnswer) {
			// 	APIquesList[j].wrongArr = APIquesList[j].tempAnswer
			// }
	
		} else if (APIquesList[j].template == 4) {
			// 4.填空题
			// if (!APIquesList[j].unAnswered) {
			// 	// 题目已经作答 获取用户填空题答案
			// 	APIquesList[j].blankArr = APIquesList[j]
			// 		.theoryExamAnswer
			// 		.shortAnswer.split("||~||")
			// 		APIquesList[j].blankString = APIquesList[j].blankArr.join(',')
			// } else {
			// 	// 题目未作答
			// 	APIquesList[j].blankArr = [] //新建blankArr属性，临时保存填空题答案
			// 	APIquesList[j].blankString = '' //新建blankArr属性，临时保存填空题答案
			// 	APIquesList[j].theoryExamAnswer = {}
			// 	for (let m = 0; m < APIquesList[j].options.length; m++) {
			// 		APIquesList[j].theoryExamAnswer.shortAnswer += "暂未作答||~||"
			// 	}
			// }
	
			// 为填空题附上正确答案
			for (let k = 0; k < APIquesList[j].toptions.length; k++) {
				// 把null赋值为空字符串
				if (APIquesList[j].rightAnswer == '') {
					APIquesList[j].rightAnswer = APIquesList[j].toptions[k].optContent
				} else {
					APIquesList[j].rightAnswer += ',' + APIquesList[j].toptions[k].optContent
				}
			}
		} else {
			// 5.简答题
			// if (!APIquesList[j].unAnswered) {
			// 	// 题目已经作答 获取用户的简答题答案
			// 	APIquesList[j].tempAnswer = APIquesList[j]
			// 		.theoryExamAnswer
			// 		.shortAnswer
			// } else {
			// 	APIquesList[j].theoryExamAnswer = {}
			// 	APIquesList[j].theoryExamAnswer.shortAnswer = "暂未作答"
			// }
	
			// 正确答案
			APIquesList[j].rightAnswer = APIquesList[j].queAnswer
		}
		// --------------------------
		questionList.push(APIquesList[j])
	}
	store.state.questionList = questionList
}
// 上传图像
export const uploadImg = function(type) {
	return new Promise((resolve, reject) => {
		var baseURL = 'http://61.132.233.226:9001/escloud/ws2'
		var restURL = ''
		var successTitle = '图片上传成功'
		var failTitle = '图片上传失败'
		if (type == "portrait") {
			// 修改头像
			restURL = '/system/upload'
			successTitle = '修改头像成功'
			failTitle = '修改头像失败'
		} else if (type == "face") {
			// 人脸识别
			restURL = '/system/checkFace'
			successTitle = '人脸识别通过'
			failTitle = '人脸识别未通过'
		} else {}
		uni.chooseImage({
			count: 1, //上传图像的数量
			sizeType: ['original', 'compressed'],
			sourceType: ['album', 'camera'],
			success(res) {
				// tempFilePath可以作为img标签的src属性显示图片
				// console.log('tempFilePaths：', JSON.stringify(res.tempFilePaths));
				// console.log('tempFiles：', res.tempFiles);

				uni.uploadFile({
					url: baseURL + restURL,
					filePath: res.tempFilePaths[0],
					name: 'pic',
					header: {
						Authorization: 'Bearer ' + store.state.access_token,
					},
					success(res) {
						console.log(res);
						if (res.data.indexOf('操作成功') >= 0) {
							uni.showToast({
								title: successTitle,
								duration: 2000
							});
							console.log("图片上传操作成功");
							// store.state.passFace = true
							resolve(res)
						} else {
							uni.showToast({
								title: failTitle,
								duration: 2000,
								image: '../../static/error.png'
							});
							console.log("操作失败");
							resolve(res)
						}
					},
					fail(err) {
						console.log(err);
						uni.showToast({
							title: '图片上传失败',
							duration: 2000,
							image: '../../static/error.png'
						});
						reject(err)
					},
				})
			}
		});
	})
}


// 人脸识别 上传图像
export const uploadImg2 = function(type) {
	return new Promise((resolve, reject) => {
		var baseURL = 'http://61.132.233.226:9001/escloud/ws2'
		var restURL = ''
		var successTitle = '图片上传成功'
		var failTitle = '图片上传失败'
		if (type == "portrait") {
			// 修改头像
			restURL = '/system/upload'
			successTitle = '修改头像成功'
			failTitle = '修改头像失败'
		} else if (type == "face") {
			// 人脸识别
			restURL = '/system/checkFace'
			successTitle = '人脸识别通过'
			failTitle = '人脸识别未通过'
		} else {}
		uni.chooseImage({
			count: 1, //上传图像的数量
			sizeType: ['original', 'compressed'],
			sourceType: ['camera'],
			success(res) {
				// tempFilePath可以作为img标签的src属性显示图片
				// console.log('tempFilePaths：', JSON.stringify(res.tempFilePaths));
				// console.log('tempFiles：', res.tempFiles);

				uni.uploadFile({
					url: baseURL + restURL,
					filePath: res.tempFilePaths[0],
					name: 'pic',
					header: {
						Authorization: 'Bearer ' + store.state.access_token,
					},
					success(res) {
						console.log(res);
						if (res.data.indexOf('操作成功') >= 0) {
							uni.showToast({
								title: successTitle,
								duration: 2000
							});
							console.log("图片上传操作成功");
							// store.state.passFace = true
							resolve(res)
						} else {
							uni.showToast({
								title: failTitle,
								duration: 2000,
								image: '../../static/error.png'
							});
							console.log("操作失败");
							resolve(res)
						}
					},
					fail(err) {
						console.log(err);
						uni.showToast({
							title: '图片上传失败',
							duration: 2000,
							image: '../../static/error.png'
						});
						reject(err)
					},
				})
			}
		});
	})
}
