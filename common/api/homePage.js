import {
	get,
	post
} from '../$http'
import store from '@/store/index.js'; //需要引入store
import axios from 'axios';

// store.state.access_token
// 获取消息列表
export const myNotice = function(currentPage,title) {
	return post('/stuNotice/myNotice', {
		current: currentPage,
		"search": `{
			"title":'${title}'
		}`,
	}, store.state.access_token)
}
// 批量删除消息列表
export const deleteNotice = function(ids) {
	return post('/stuNotice/myNotice/deleteBatch', {
		ids:ids
	}, store.state.access_token)
}
// 批量已读消息
export const readNotice = function(ids) {
	return post('/stuNotice/myNotice/read', {
		ids:ids
	}, store.state.access_token)
}

// 修改密码
export const changePass = function(pwd, vaPwd, vaPwdA) {
	return post('/system/updatePsw', {
		password: pwd,
		validatePassword: vaPwd,
		validatePasswordAgain: vaPwdA
	}, store.state.access_token)
}

// 获取学生信息
export const userInfo = function(params) {
	return get('/system/userMessage', params, store.state.access_token)
}

// 保存学生信息
export const saveInfo = function(params) {
	return post('/system/userMessage', {
		// realName: params,
		realName: params.realName,
		address: params.address,
		phone: params.phone,
		email: params.email,
		userBir: params.userBir
	}, store.state.access_token)
}

// 修改头像
export const updatePortrait = function(picObj) {
	return post('/system/upload', {
		pic:picObj
	}, store.state.access_token)
}