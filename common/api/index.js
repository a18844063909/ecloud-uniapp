// 批量导出文件
const requireApi = require.context(
	// api 目录的相对路径
	'.',
	// 是否查询子目录
	false,
	// 查询文件的一个后缀
	/.js$/
)
/*
循环读取api文件夹下的js文件，将个文件export出的内容统一加到module对象上
然后再导出module，将module再挂载到vue实例
*/
let module = {}
requireApi.keys().forEach((key,index)=>{
	if(key === './index.js') return
	console.log(key);
	Object.assign(module,requireApi(key))
})

export default module