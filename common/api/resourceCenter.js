import {
	get,
	post
} from '../$http'

export const resourceCenterget = function(token) {
	return get('/resource/resourceCenter', null, token)
}

export const resourceCenterpost = function(token, num, page) {
	return post('/resource/resourceCenter', {
		limit: num,
		current: page
	}, token)
}

export const checkResource = function(token, id) {
	return get('/resource/resourceCenter/check', {
		id: id
	}, token)
}

export const comment = function(token, id, comment, grade) {
	return post('/resource/resourceCenter/comment', {
		resourceId: id,
		comment: comment,
		grade: grade
	}, token)
}

export const getComments = function(token, id, num, page) {
	return post('/resource/resourceCenter/getComments', {
		search: `{"resourceId":${id}}`,
		limit: num,
		current: page
	}, token)
}

export const myDownloadget = function(token) {
	return get('/resource/myDownload', null, token)
}

export const myDownloadpost = function(token, num, page) {
	return post('/resource/myDownload', {
		limit: num,
		current: page
	}, token)
}

export const myFavoriteget = function(token) {
	return get('/resource/myFavorite', null, token)
}

export const myFavoritepost = function(token, num, page) {
	return post('/resource/myFavorite', {
		limit: num,
		current: page
	}, token)
}

export const myCommentget = function(token) {
	return get('/resource/myComment', null, token)
}

export const myCommentpost = function(token, num, page) {
	return post('/resource/myComment', {
		limit: num,
		current: page
	}, token)
}

export const myCommentDelete = function(token, deleteID) {
	return post('resource/myComment/deleteBatch', {
		ids: deleteID
	}, token)
}

export const myCommentCheck = function(token, resourceId) {
	return get('/resource/myComment/check', {
		id: resourceId
	}, token)
}

export const collection = function(token, id, favorChoice) {
	return post('/resource/resourceCenter/collection', {
		resourceId: id,
		index: favorChoice
	}, token)
}

export const resourceCenterSearch = function(token, searchObj, num, page) {
	let search = `{"name":"${searchObj.name}","type":"${searchObj.type}","subjectList":"${searchObj.subjectList}","format":"${searchObj.format}","sort":"${searchObj.sortOP}","sortName":"${searchObj.sortName}"}`
	console.log(search)
	return post('/resource/resourceCenter', {
		search:search,
		limit: num,
		current: page
	}, token)
}

export const favoriteDeleteBatch = function(token, id) {
	return post('/resource/myFavorite/deleteBatch', {
		ids: id
	}, token)
}